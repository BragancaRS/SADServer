package model;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ThreadClient extends Thread{

    private Socket srvSocket;
    private int port;

    public ThreadClient() {

    }

    private static Object getObjectFromByte(byte[] objectAsByte) {
        Object obj = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        try {
            bis = new ByteArrayInputStream(objectAsByte);
            ois = new ObjectInputStream(bis);
            obj = ois.readObject();

            bis.close();
            ois.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return obj;

    }

    public ThreadClient(Socket clie) {
        this.srvSocket = clie;
    }

    public void setSrvSocket(Socket srvSocket) {
        this.srvSocket = srvSocket;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Socket getSrvSocket() {
        return srvSocket;
    }

    public int getPort() {
        return port;
    }

    @Override
    public void run() {
        try {
            //1
            ServerSocket srvSocket = new ServerSocket(port);
            System.out.println("Aguardando envio de arquivo ...");
            Socket socket = srvSocket.accept();

            //2
            byte[] objectAsByte = new byte[socket.getReceiveBufferSize()];
            BufferedInputStream bf = new BufferedInputStream(
                    socket.getInputStream());
            bf.read(objectAsByte);

            //3
            File arquivo = (File) getObjectFromByte(objectAsByte);

            //4
            String dir = arquivo.getDiretorioDestino().endsWith("/") ? arquivo
                    .getDiretorioDestino() + arquivo.getNome() : arquivo
                    .getDiretorioDestino() + "/" + arquivo.getNome();
            System.out.println("Escrevendo arquivo " + dir);

            //5
            FileOutputStream fos = new FileOutputStream(dir);
            fos.write(arquivo.getConteudo());
            fos.close();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
