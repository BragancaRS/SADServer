package controller;

import java.net.ServerSocket;
import java.net.Socket;
import model.ThreadClient;

/**
 *
 * @author root
 */
public class ServerControl {

    private Socket socket;
    private int port;

    public ServerControl(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            //Converte o parametro recebido para int (número da porta)    
            System.out.println("Incializando o servidor...");
            //Iniciliza o servidor
            ServerSocket serv = new ServerSocket(port);
            System.out.println("Servidor iniciado, ouvindo a porta " + port);
            //Aguarda conexões
            while (true) {
                Socket clie = serv.accept();
                //Inicia thread do cliente
                new ThreadClient(clie).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
